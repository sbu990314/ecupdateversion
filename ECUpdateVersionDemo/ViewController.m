//
//  ViewController.m
//  ECUpdateVersionDemo
//
//  Created by PO-YU SU on 2014/7/10.
//  Copyright (c) 2014年 SBU. All rights reserved.
//

#import "ViewController.h"
#import "ECUpdateVersion.h"

@interface ViewController ()
@property (strong, nonatomic) ECUpdateVersion *updateVersion;
@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    __block UIViewController *weakSelf = self;
    
    self.updateVersion = [ECUpdateVersion ECUpdateVersionWithAppleID:@"583798880"];
    self.updateVersion.block = ^(UIAlertController *alertController)
    {
        [weakSelf presentViewController:alertController animated:YES completion:nil];
    };
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
