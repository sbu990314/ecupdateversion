//
//  main.m
//  ECUpdateVersionDemo
//
//  Created by PO-YU SU on 2014/7/10.
//  Copyright (c) 2014年 SBU. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
