//
//  ECUpdateVersion.m
//  ECUpdateVersionDemo
//
//  Created by PO-YU SU on 2014/7/10.
//  Copyright (c) 2014年 SBU. All rights reserved.
//

#import "ECUpdateVersion.h"

#define APP_URL @"http://itunes.apple.com/lookup?id=%@"

@interface ECUpdateVersion ()
@property (strong, nonatomic) NSString *trackViewUrl;
@property (strong, nonatomic) NSString *updateVer;
@end

@implementation ECUpdateVersion

+ (instancetype)ECUpdateVersionForceWithAppleID:(NSString *)appleID log:(NSString *)log
{
    ECUpdateVersion *updateVersion = [[ECUpdateVersion alloc] init];
    [updateVersion forceUpdateVersionWithAppleID:appleID log:log];
    
    return updateVersion;
}

+ (instancetype)ECUpdateVersionNowWithAppleID:(NSString *)appleID log:(NSString *)log
{
    ECUpdateVersion *updateVersion = [[ECUpdateVersion alloc] init];
    [updateVersion nowUpdateVersionWithAppleID:appleID log:log];
    
    return updateVersion;
}

+ (instancetype)ECUpdateVersionWithAppleID:(NSString *)appleID log:(NSString *)log
{
    ECUpdateVersion *updateVersion = [[ECUpdateVersion alloc] init];
    [updateVersion updateVersionWithAppleID:appleID log:log];
    
    return updateVersion;
}

+ (instancetype)ECUpdateVersionWithAppleID:(NSString *)appleID
{
    ECUpdateVersion *updateVersion = [[ECUpdateVersion alloc] init];
    [updateVersion updateVersionWithAppleID:appleID log:nil];
    
    return updateVersion;
}

- (NSString *)localizedStringKey:(NSString *)key
{
    return NSLocalizedStringFromTable(key, @"ECUpdateVersion", nil);
}

#pragma mark - private
- (void)forceUpdateVersionWithAppleID:(NSString *)appleID log:(NSString *)log
{
    NSString *urlString = [NSString stringWithFormat:APP_URL, appleID];
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue currentQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
     {
         if (connectionError)
             return;
         
         NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
         if (!jsonDictionary)
             return;
         
         NSArray *results = [jsonDictionary objectForKey:@"results"];
         if (!results)
             return;
         
         if (results.count == 0)
             return;
         
         NSDictionary *appDictionary = [results objectAtIndex:0];
         if (!appDictionary)
             return;
         
         NSString *newVer = [appDictionary objectForKey:@"version"];
         NSString *nowVer = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
         if([newVer compare:nowVer options:NSNumericSearch] != NSOrderedDescending)
             return;
         
         NSString *releaseNotes = [appDictionary objectForKey:@"releaseNotes"];
         NSString *trackViewUrl = [appDictionary objectForKey:@"trackViewUrl"];
         
         NSString *title = [NSString stringWithFormat:@"%@ (%@)",[self localizedStringKey:@"ECUpdateVersion_NewVersion"], newVer];
         UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title
                                                                                  message:(log)?log:releaseNotes
                                                                           preferredStyle:UIAlertControllerStyleAlert];
         
         UIAlertAction *updateAction = [UIAlertAction actionWithTitle:[self localizedStringKey:@"ECUpdateVersion_Updated"]
                                                                style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * _Nonnull action)
                                        {
                                            NSURL *url = [NSURL URLWithString:self.trackViewUrl];
                                            [[UIApplication sharedApplication] openURL:url];
                                        }];
         
         UIAlertAction *tempAction = [UIAlertAction actionWithTitle:[self localizedStringKey:@"ECUpdateVersion_Temporarily"]
                                                              style:UIAlertActionStyleCancel
                                                            handler:^(UIAlertAction * _Nonnull action)
                                      {
                                          exit(0);
                                      }];
         
         [alertController addAction:updateAction];
         [alertController addAction:tempAction];
         
         self.updateVer = newVer;
         self.trackViewUrl = trackViewUrl;
     }];
}

- (void)nowUpdateVersionWithAppleID:(NSString *)appleID log:(NSString *)log
{
    NSString *urlString = [NSString stringWithFormat:APP_URL, appleID];
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue currentQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
     {
         if (connectionError)
             return;
         
         NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
         if (!jsonDictionary)
             return;
         
         NSArray *results = [jsonDictionary objectForKey:@"results"];
         if (!results)
             return;
         
         if (results.count == 0)
             return;
         
         NSDictionary *appDictionary = [results objectAtIndex:0];
         if (!appDictionary)
             return;
         
         NSString *newVer = [appDictionary objectForKey:@"version"];
         NSString *nowVer = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
         if([newVer compare:nowVer options:NSNumericSearch] != NSOrderedDescending)
         {
             UIAlertController *alertController = [UIAlertController alertControllerWithTitle:[self localizedStringKey:@"ECUpdateVersion_Message"]
                                                                                      message:[self localizedStringKey:@"ECUpdateVersion_LatestVersion"]
                                                                               preferredStyle:UIAlertControllerStyleAlert];
             
             UIAlertAction *action = [UIAlertAction actionWithTitle:[self localizedStringKey:@"ECUpdateVersion_Done"]
                                                              style:UIAlertActionStyleCancel
                                                            handler:^(UIAlertAction * _Nonnull action)
             {
                 
             }];
             
             [alertController addAction:action];
             
             if (self.block)
             {
                 self.block(alertController);
             }

             return;
         }
         
         NSString *releaseNotes = [appDictionary objectForKey:@"releaseNotes"];
         NSString *trackViewUrl = [appDictionary objectForKey:@"trackViewUrl"];
         
         NSString *title = [NSString stringWithFormat:@"%@ (%@)",[self localizedStringKey:@"ECUpdateVersion_NewVersion"], newVer];
         UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title
                                                                                  message:(log)?log:releaseNotes
                                                                           preferredStyle:UIAlertControllerStyleAlert];
         
         UIAlertAction *updateAction = [UIAlertAction actionWithTitle:[self localizedStringKey:@"ECUpdateVersion_Updated"]
                                                                style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * _Nonnull action)
                                        {
                                            NSURL *url = [NSURL URLWithString:self.trackViewUrl];
                                            [[UIApplication sharedApplication] openURL:url];
                                        }];
         
         UIAlertAction *tempAction = [UIAlertAction actionWithTitle:[self localizedStringKey:@"ECUpdateVersion_Temporarily"]
                                                              style:UIAlertActionStyleCancel
                                                            handler:^(UIAlertAction * _Nonnull action)
                                      {

                                      }];
         
         [alertController addAction:updateAction];
         [alertController addAction:tempAction];
         
         self.updateVer = newVer;
         self.trackViewUrl = trackViewUrl;
         
         if (self.block)
         {
             self.block(alertController);
         }
     }];
}

- (void)updateVersionWithAppleID:(NSString *)appleID log:(NSString *)log
{
    NSString *urlString = [NSString stringWithFormat:APP_URL, appleID];
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue currentQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
     {
         if (connectionError)
             return;
         
         NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
         if (!jsonDictionary)
             return;
         
         NSArray *results = [jsonDictionary objectForKey:@"results"];
         if (!results)
             return;
         
         if (results.count == 0)
             return;
         
         NSDictionary *appDictionary = [results objectAtIndex:0];
         if (!appDictionary)
             return;
         
         NSString *newVer = [appDictionary objectForKey:@"version"];
         NSString *ignoreVer = [self readIgnoreVersion];
         if ([newVer compare:ignoreVer options:NSNumericSearch] == NSOrderedSame)
             return;
         
         NSString *nowVer = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
         if([newVer compare:nowVer options:NSNumericSearch] != NSOrderedDescending)
         {
             UIAlertController *alertController = [UIAlertController alertControllerWithTitle:[self localizedStringKey:@"ECUpdateVersion_Message"]
                                                                                      message:[self localizedStringKey:@"ECUpdateVersion_LatestVersion"]
                                                                               preferredStyle:UIAlertControllerStyleAlert];
             
             UIAlertAction *action = [UIAlertAction actionWithTitle:[self localizedStringKey:@"ECUpdateVersion_Done"]
                                                              style:UIAlertActionStyleCancel
                                                            handler:^(UIAlertAction * _Nonnull action)
                                      {
                                          
                                      }];
             
             [alertController addAction:action];
             
             if (self.block)
             {
                 self.block(alertController);
             }
             
             return;
         }
         
         
         NSString *releaseNotes = [appDictionary objectForKey:@"releaseNotes"];
         NSString *trackViewUrl = [appDictionary objectForKey:@"trackViewUrl"];
         
         NSString *title = [NSString stringWithFormat:@"%@ (%@)",[self localizedStringKey:@"ECUpdateVersion_NewVersion"], newVer];
         UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title
                                                                                  message:log?log:releaseNotes
                                                                           preferredStyle:UIAlertControllerStyleAlert];
         UIAlertAction *updateAction = [UIAlertAction actionWithTitle:[self localizedStringKey:@"ECUpdateVersion_Updated"]
                                                                style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * _Nonnull action)
                                        {
                                            NSURL *url = [NSURL URLWithString:self.trackViewUrl];
                                            [[UIApplication sharedApplication] openURL:url];
                                        }];

         UIAlertAction *againAction = [UIAlertAction actionWithTitle:[self localizedStringKey:@"ECUpdateVersion_Remind"]
                                                               style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * _Nonnull action)
                                       {
                                           
                                       }];
         
         UIAlertAction *ignoreAction = [UIAlertAction actionWithTitle:[self localizedStringKey:@"ECUpdateVersion_Ignore"]
                                                                style:UIAlertActionStyleCancel
                                                              handler:^(UIAlertAction * _Nonnull action)
                                        {
                                            [self writeIgnoreVersion:self.updateVer];
                                        }];
         
         [alertController addAction:updateAction];
         [alertController addAction:againAction];
         [alertController addAction:ignoreAction];
         
         self.updateVer = newVer;
         self.trackViewUrl = trackViewUrl;
         
         if (self.block)
         {
             self.block(alertController);
         }
     }];
}

- (NSString *)readIgnoreVersion
{
    NSString *document = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *path = [document stringByAppendingPathComponent:@"ignoreVersion.txt"];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:path])
    {
        return @"";
    }
    
    return [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
}

- (void)writeIgnoreVersion:(NSString *)version
{
    NSString *document = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *path = [document stringByAppendingPathComponent:@"ignoreVersion.txt"];
    
    [version writeToFile:path atomically:YES encoding:NSUTF8StringEncoding error:nil];
}

@end
