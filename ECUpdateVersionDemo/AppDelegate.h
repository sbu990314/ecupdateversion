//
//  AppDelegate.h
//  ECUpdateVersionDemo
//
//  Created by PO-YU SU on 2014/7/10.
//  Copyright (c) 2014年 SBU. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
