//
//  ECUpdateVersion.h
//  ECUpdateVersionDemo
//
//  Created by PO-YU SU on 2014/7/10.
//  Copyright (c) 2014年 SBU. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^ECUpdateVersionBlock)(UIAlertController *alertController);

@interface ECUpdateVersion : NSObject

@property (strong, nonatomic) ECUpdateVersionBlock block;
+ (instancetype)ECUpdateVersionForceWithAppleID:(NSString *)appleID log:(NSString *)log;
+ (instancetype)ECUpdateVersionNowWithAppleID:(NSString *)appleID log:(NSString *)log;
+ (instancetype)ECUpdateVersionWithAppleID:(NSString *)appleID log:(NSString *)log;
+ (instancetype)ECUpdateVersionWithAppleID:(NSString *)appleID;
@end
